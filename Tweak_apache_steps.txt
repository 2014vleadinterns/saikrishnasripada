There are different importance levels you can use, debug, info, warning, error and critical.

Changing the log level:
logging.basicConfig(level=logging.DEBUG)

Basic Logger program

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a file handler
handler = logging.FileHandler('hello.log')
handler.setLevel(logging.INFO)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)
logger.info('Hello baby')


tailf implementation in python:

import time
while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        time.sleep(1)
        file.seek(where)
    else:
        print line, # already has newline

The method tell() returns the current position of the file read/write pointer within the file.

Accessing apache2/access.log :

1. tokenize each line of the access.log to tuple.

import re
line = '172.16.0.3 - - [25/Sep/2002:14:04:19 +0200] "GET / HTTP/1.1" 401 - "" "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.1) Gecko/20020827"'
regex = '([(\d\.)]+) - - \[(.*?)\] "(.*?)" (\d+) - "(.*?)" "(.*?)"'
print re.match(regex, line).groups()
tuplea = re.match(regex, line).groups()
print tuplea[1]

 The regex may vary basing on the format of the line.

2. Read the apache2/access.log

Print the first and last lines of a file:

with open(fname, 'rb') as fh:
    first = next(fh)
    offs = -100
    while True:
        fh.seek(offs, 2)
        lines = fh.readlines()
        if len(lines)&gt;1:
            last = lines[-1]
            break
        offs *= 2
    print first
    print last


3. Comparing time of apache accesslog format:

import time, datetime

def date_compare(ts1, ts2, **compkw):
    t1 = time.strptime(ts1.strip('[]').split()[0], '%d/%b/%Y:%H:%M:%S')
    d1 = datetime.datetime(*t1[:6])
    t2 = time.strptime(ts2.strip('[]').split()[0], '%d/%b/%Y:%H:%M:%S')
    d2 = datetime.datetime(*t2[:6])
    delta = d2-d1
    orderList = ['days', 'hours', 'minutes', 'seconds']
    outList = []
    for item in orderList:
        if compkw.has_key(item):
            outList.append('%s %s' % (compkw[item], item))
    outStr = ', '.join(outList)
    if delta &lt;= datetime.timedelta(**compkw):
        return 'Less than or equal to %s' % (outStr)
    return 'Greater than %s' % (outStr)

http://bytes.com/topic/python/answers/778342-comparing-apache-timestamps

4. Comparing the current time with apache2/access.log time format:

The above function will be useful

import datetime
date_compare(timestamp, datetime.datetime.now().strftime('%d/%b/%Y:%H:%M:%S') + ' +0530', seconds = 30):

 

  