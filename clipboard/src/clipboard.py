import sys
import tempfile

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__size__ = 10
__sizeBlob__ = 12000
temp
tempblob = tempfile.NamedTemporaryFile()

def copytext(text):
    global __text__ 
    global __size__
    global temp
                                                                    #insert temp.reset()
    temp = tempfile.NamedTemporaryFile()
    if len(text) > __size__:
        temp.
        temp.write(text)
        __text__ = temp.name
        temp.seek(0)
    elif len(text) <= __size__:
        __text__ = text

def copyblob(blob):
    global __blob__
    global __sizeBlob__
    global tempblob

    if len(blob) > __sizeBlob__:
        tempblob.write(blob)
        __blob__ = tempblob.name
        tempblob.seek(0)
    elif len(blob) <=sizeBlob__ :
        __blob__ = blob


def gettext():
    global __text__
    global temp
    temp.seek(0)
    if len(temp.read()) > 0:
        temp.seek(0)
        return temp.read()
    else:
        return __text__

def getblob():
    global __blob__
    global tempblob
    tempblob.seek(0)
    if len(tempblob.read()) > 0:
        tempblob.seek(0)
        return tempblob.read()
    else:
        return __blob__
#    global __blob__
#    return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
