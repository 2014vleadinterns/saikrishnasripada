import sys
import tempfile

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__size__ = 10
__sizeBlob__ = 1200

temp = None
tempblob = None

__textreadsize__ =None
__blobreadsize__ =None

def copytext(text):
    global __text__
    global __size__
    global temp
    global __textreadsize__
    __textreadsize__ = len(text)
    if len(text) > __size__:
        if temp is not None :
            temp.close()
            temp1 = tempfile.NamedTemporaryFile()
            temp =temp1
        else :
            temp = tempfile.NamedTemporaryFile()
        temp.write(text)
        __text__ = temp.name
        temp.seek(0)
    elif len(text) <= __size__:
        __text__ = text

def copyblob(blob):
    global __blob__
    global __sizeBlob__
    global tempblob
    global __blobreadsize__
    __blobreadsize__ = len(blob)
    if len(blob) > __sizeBlob__:
        if tempblob is not None :
            tempblob.close()
            temp1 = tempfile.NamedTemporaryFile()
            tempblob =temp1
        else :
            tempblob = tempfile.NamedTemporaryFile()
        tempblob.write(blob)
        __blob__ = tempblob.name
        tempblob.seek(0)
    elif len(blob) <= __sizeBlob__:
        __blob__ = blob


def gettext():
    global __text__
    global __size__
    global temp
    global __textreadsize__
    if __textreadsize__ > __size__:
        temp.seek(0)
        return temp.read()
    else:
        return __text__

def getblob():
    global __blob__
    global __sizeBolb__
    global tempblob
    global __blobreadsize__
    if __blobreadsize__  > __sizeBolb__:
        tempblob.seek(0)
        return tempblob.read()
    else:
        return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None

